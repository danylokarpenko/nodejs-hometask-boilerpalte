exports.fight = {
    id: (body) => {
        if (body.id) {
            return false;
        }
        return true;
    },
    fighter1: (body) => {
        if (!body.fighter1) {
            return false;
        }
        return true;
    },
    fighter2: (body) => {
        if (!body.fighter2) {
            return false;
        }
        return true;
    },
    log: (body) => {
        if (!body.log) {
            return false;
        }
        if (body.log.length === 0) {
            return false;
        }
        return true;
    }
}