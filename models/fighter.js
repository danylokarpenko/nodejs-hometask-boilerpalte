exports.fighter = {
    id: (body) => {
        if (body.id) {
            return false;
        }
        return true;
    },
    name: (body) => {
        const { name } = body;
        if (!name || name === 'undefined') {
            return false;
        }
        if (name.length < 2) {
            return false;
        }
        return true;
    },
    health: (body) => {
        const { health } = body;
        if (!health || health === 'undefined') {
            return false;
        }
        if (!Number.isInteger(health) || health !== 100) {
            return false;
        }
        return true;
    },
    defense: (body) => {
        const { defense } = body;
        if (!defense || defense === 'undefined') {
            return false;
        }
        if (!Number.isInteger(defense)) {
            return false;
        }
        if (defense < 1 || defense > 10) {
            return false;
        }
        return true;
    },
    power: (body) => {
        const { power } = body;
        if (!power || power === 'undefined') {
            return false;
        }
        if (!Number.isInteger(power)) {
            return false;
        }
        if (power < 1 || power > 100) {
            return false;
        }
        return true;
    }
}