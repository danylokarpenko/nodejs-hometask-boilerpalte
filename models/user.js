exports.user = {
    id: (body) => {
        if (body.id) {
            return false;
        }
        return true;
    },
    firstName: (body) => {    
        const { firstName } = body;    
        if (!firstName || firstName === 'undefined') {
            return false;
        }
        if (firstName.length < 2) {
            return false;
        }
        return true;
    },
    lastName: (body) => {
        const { lastName } = body;
        if (!lastName || lastName === 'undefined') {
            return false;
        }
        if (lastName.length < 2) {
            return false;
        }
        return true;
    },
    email: (body) => {
        const { email } = body;
        if (!email || email === 'undefined') {
            return false;
        }
        if (!email.includes('@gmail')) {
            return false;
        }
        return true;
    },
    phoneNumber: (body) => {
        const { phoneNumber } = body;
        if (!phoneNumber || phoneNumber === 'undefined') {
            return false;
        }
        if (phoneNumber.length !== 13) {
            return false;
        }
        if (phoneNumber.slice(0, 4) !== '+380') {
            return false;
        }
        return true;
    },
    password: (body) => {
        const { password } = body;
        if (!password || password === 'undefined') {
            return false;
        }
        if (password.length < 3) {
            return false;
        }
        return true;
    }
}