const request = require('supertest');
const faker = require('faker');

const { app } = require('..');

const generateUserData = (props = {}) => {
    const defaultProps = {
        email: faker.internet.email('danylo', 'karpenko', 'gmail.com').toLowerCase(),
        password: faker.internet.password(),
        phoneNumber: '+380660288954',
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
    };
    return Object.assign({}, defaultProps, props);
};

const generateFighterData = (props = {}) => {
    const defaultProps = {
        health: 100,
        power: Math.floor(Math.random() * 100) + 1,
        defense: Math.floor(Math.random() * 10) + 1,
        name: faker.name.firstName(),
    };
    return Object.assign({}, defaultProps, props);
};

describe('App enter point', async () => {
    it('GET / 200', async () => {
        const res = await request(app)
            .get('/');
        expect(res.statusCode).toBe(200);
    });
    it('GET /wrong 404', async () => {
        const res = await request(app)
            .get('/wrong-path');
        expect(res.statusCode).toBe(404);
    });
})

describe('Fighter entity', async () => {
    const fighterData = generateFighterData();

    it('CREATE fighter 200', async () => {
        const res = await request(app)
            .post('/api/fighters')
            .send(fighterData);
        expect(res.statusCode).toBe(200);
    });
    it('GET /api/fighters 200', async () => {
        const res = await request(app)
            .get('/api/fighters');
        expect(res.statusCode).toBe(200);
    });
    it('CREATE fighter 400: name: undefined', async () => {
        const res = await request(app)
            .post('/api/fighters')
            .send({ ...fighterData, name: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE fighter 400: defense: undefined', async () => {
        const res = await request(app)
            .post('/api/fighters')
            .send({ ...fighterData, defense: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE fighter 400: Unexpected field passed', async () => {
        const res = await request(app)
            .post('/api/fighters')
            .send({ ...fighterData, unExpected: "Must not write" });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE fighter 400: id field passed', async () => {
        const res = await request(app)
            .post('/api/fighters')
            .send({ ...fighterData, id: "3j8-4hj-2sl" });
        expect(res.statusCode).toBe(400);
    });
    it('UPDATE fighter 200: ', async () => {
        const { body: fighter } = await request(app)
            .post('/api/fighters')
            .send(fighterData)

        const res = await request(app)
            .put(`/api/fighters/${fighter.id}`)
            .send({ name: "Dart" });
        expect(res.statusCode).toBe(200);
    });
    it('UPDATE fighter 400: ', async () => {
        const { body: fighter } = await request(app)
            .post('/api/fighters')
            .send(fighterData)

        const res = await request(app)
            .put(`/api/fighters/${fighter.id}`)
            .send({ love: "true" });
        expect(res.statusCode).toBe(400);
    });
    it('UPDATE fighter 200: ', async () => {
        const { body: fighter } = await request(app)
            .post('/api/fighters')
            .send(fighterData)

        const res = await request(app)
            .put(`/api/fighters/unknownId`)
            .send({ name: "Dart" });
        expect(res.statusCode).toBe(404);
    });
})
describe('User entity', async () => {
    const userData = generateUserData();

    it('CREATE user 200', async () => {
        const res = await request(app)
            .post('/api/users')
            .send(userData);
        expect(res.statusCode).toBe(200);
    });
    it('CREATE USER 400: firstName: undefined', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, firstName: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: lastName: undefined', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, lastName: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: password: undefined', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, password: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: email: undefined', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, email: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: phoneNumber: undefined', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, phoneNumber: undefined });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: Unexpected field passed', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, unExpected: "Must not write" });
        expect(res.statusCode).toBe(400);
    });
    it('CREATE USER 400: id field passed', async () => {
        const res = await request(app)
            .post('/api/users')
            .send({ ...userData, id: "3j8-4hj-2sl" });
        expect(res.statusCode).toBe(400);
    });
    it('UPDATE USER 200: ', async () => {
        const { body: user } = await request(app)
            .post('/api/users')
            .send(userData)

        const res = await request(app)
            .put(`/api/users/${user.id}`)
            .query({ id: user.id })
            .send({ firstName: "Jesus" });
        expect(res.statusCode).toBe(200);
    });
    it('UPDATE USER 400: ', async () => {
        const { body: user } = await request(app)
            .post('/api/users')
            .send(userData)

        const res = await request(app)
            .put(`/api/users/${user.id}`)
            .send({ firstName: "Jesus", unExpected: 'XoXoXO Will fall' });
        expect(res.statusCode).toBe(400);
    });
    it('UPDATE USER 404: ', async () => {
        const { body: user } = await request(app)
            .post('/api/users')
            .send(userData)

        const res = await request(app)
            .put(`/api/users/404-unknown`)
            .send({ firstName: "Doesnt matter" });
        expect(res.statusCode).toBe(404);
    });
    it('GET /api/users 200', async () => {
        const res = await request(app)
            .get('/api/users');
        expect(res.statusCode).toBe(200);
    });
});
