const { fighter } = require('../models/fighter');

const createFighterValid = async (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    req.body.health = 100;

    for (let prop in req.body) {
        if (!fighter[prop]) {
            res.status(400);
            res.err = Error('Fighter entity to create is not valid');
            break;
        }
    }
    for (let prop in fighter) {
        const validator = fighter[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('Fighter entity to create is not valid');
            break;
        }
    }
    next();
}

const updateFighterValid = async (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    for (let prop in req.body) {
        if (!fighter[prop]) {
            res.status(400);
            res.err = Error('Fighter entity to update is not valid');
            break;
        }
        const validator = fighter[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('Fighter entity to update is not valid');
            break;
        }
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;