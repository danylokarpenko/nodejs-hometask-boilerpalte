const { fight } = require('../models/fight');

const createFightValid = async (req, res, next) => {    
    for (let prop in req.body) {
        if (!fight[prop]) {
            res.status(400);
            res.err = Error('Fight entity to create is not valid');
            break;
        }
    }
    for (let prop in fight) {
        const validator = fight[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('Fight entity to create is not valid');
            break;
        }
    }
    next();
}

const updateFightValid = async (req, res, next) => {
    for (let prop in req.body) {
        if (!fight[prop]) {
            res.status(400);
            res.err = Error('Fight entity to update is not valid');
            break;
        }
        const validator = fight[prop];
        const isValid = validator(req.body);
        if (!isValid) {
            res.status(400);
            res.err = Error('Fight entity to update is not valid');
            break;
        }
    }
    next();
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;