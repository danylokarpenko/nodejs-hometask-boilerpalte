const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    const { err, data } = res;
    
    if (err) {
        const statusCode = res.statusCode || '418';

        res.status(statusCode).json({
            error: true,
            message: err.message,
        })
    }
    res.status(200).json(data)
    next();
}

exports.responseMiddleware = responseMiddleware;