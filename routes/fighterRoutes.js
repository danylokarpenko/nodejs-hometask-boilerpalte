const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router
    .get('/', (req, res, next) => {        
        try {
            const fighters = FighterService.findAll();
            res.data = fighters;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .get('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const fighter = FighterService.search({ id });
            res.data = fighter;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .post('/', createFighterValid, (req, res, next) => {
        if (res.err) return next();

        const { body: fighterData } = req;     
        try {
            const fighter = FighterService.create(fighterData);
            res.data = fighter;
        } catch (err) {
            res.status(400);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .put('/:id', updateFighterValid, (req, res, next) => {
        if (res.err) {
            return next();
        }
        
        const { body: fighterData, params: { id } } = req;        
        try {
            const updatedFighter = FighterService.update(id, fighterData);
            res.data = updatedFighter;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .delete('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const response = FighterService.delete(id);
            res.data = response;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)

module.exports = router;