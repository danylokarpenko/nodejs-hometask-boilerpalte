const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router
    .get('/', (req, res, next) => {
        try {
            const fights = FightService.findAll();
            res.data = fights;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .get('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const fighter = FightService.search({ id });
            res.data = fighter;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .post('/', createFightValid, (req, res, next) => {
        if (res.err) return next();

        const { body: fightData } = req;
        try {
            const fighter = FightService.create(fightData);
            res.data = fighter;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .put('/:id', updateFightValid, (req, res, next) => {
        if (res.err) return next();

        const { body: fightData, params: { id } } = req;
        try {
            const updatedFighter = FightService.update(id, fightData);
            res.data = updatedFighter;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)
    .delete('/:id', (req, res, next) => {
        const { id } = req.params;
        try {
            const response = FightService.delete(id);
            res.data = response;
        } catch (err) {
            res.status(404);
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware)

module.exports = router;