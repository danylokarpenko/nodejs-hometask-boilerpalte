import React from 'react';

import { getFights } from '../../services/domainRequest/fightsRequest';
import { Button } from '@material-ui/core';

import './style.css'
import RenderHistory from './renderHistory';
import History from './history';

class FightHistory extends React.Component {
    state = {
        fights: [],
        fight: null,
        visible: false
    };

    async componentDidMount() {
        const fights = await getFights();
        if(fights && !fights.error) {
            this.setState({ fights });
        }
    }

    onFightSelect = (fight) => {        
        this.setState({ fight });
    }

    getFightList = () => {
        const { fight, fights } = this.state;
        if(!fight) {
            return fights;
        }

        return fights.filter(it => it.id !== fight.id);
    }
    onShowFight = () => {
        this.setState({ visible: true })
    }
    onCloseFight = () => {
        this.setState({ visible: false, fight: null })
    }
    render() {
        const  { fight, visible } = this.state; 
        console.log('visible', visible, fight);
        
        return (
            <div id="history-wrapper">
                <div id="show-fight-history">
                    <History selectedFight={fight} onFightSelect={this.onFightSelect} fightList={this.getFightList() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onShowFight} variant="contained" color="primary">Show fight data</Button>
                        <Button onClick={this.onCloseFight} variant="contained" color="secondary">Close fight data</Button>
                    </div>
                    <RenderHistory visible={visible} fight={fight || {}}/>
                </div>
            </div>
        );
    }
}

export default FightHistory;