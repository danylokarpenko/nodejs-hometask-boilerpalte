import React from 'react';
import { ListItem } from '@material-ui/core';

import './style.css'

class RenderHistory extends React.Component {
    state = {
        logs: this.props.fight.log,
        visible: this.props.visible
    }

    render() {
        const { fight, visible } = this.props;
        
        if (visible && fight) {
            const { log } = fight
            return (<div id='show-fight-history'>
                {log.map((log) => {
                    const buitifyed = JSON.stringify(log, null, 2);
                    return (<ListItem >{buitifyed}</ListItem>);
                })}
            </div>)
        } else {
            return (<div></div>)
        }

    }
}

export default RenderHistory;