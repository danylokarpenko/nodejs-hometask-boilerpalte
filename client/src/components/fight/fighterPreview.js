import { createElement } from '../../helpers/domHelper';

import './styles/styles.css';

export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    if (fighter) {
        const fighterIm = createFighterImage(fighter);
        const fighterName = createFighterName(fighter);
        const fighterHealth = createFighterHealth(fighter);
        const fighterDefense = createFighterDefense(fighter);
        const fighterAttack = createFighterAttack(fighter);

        fighterElement.append(fighterIm, fighterName, fighterHealth, fighterDefense, fighterAttack)
    }

    return fighterElement;
}

function createFighterName(fighter) {
    const { name } = fighter;
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    nameElement.innerText = `Name: ${name}`;

    return nameElement;
}
function createFighterHealth(fighter) {
    const { health } = fighter;
    const healthElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    healthElement.innerText = `Health: ${health}`;

    return healthElement;
}
function createFighterDefense(fighter) {
    const { defense } = fighter;
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    defenseElement.innerText = `Defense: ${defense}`;

    return defenseElement;
}
function createFighterAttack(fighter) {
    const { attack } = fighter;
    const attackElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    attackElement.innerText = `Attack: ${attack}`;

    return attackElement;
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif',
        title: name,
        alt: name,
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}