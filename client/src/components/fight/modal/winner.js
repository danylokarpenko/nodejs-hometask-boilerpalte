import { createElement } from '../../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter, onClose) {
  const { name } = fighter;
  const title = `${name} is the winner!`;

  const attributes = {
    src: 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif',
    title,
    alt: `winner-${name}`,
  };
  const bodyElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });
  showModal({ title, bodyElement, onClose });
}
