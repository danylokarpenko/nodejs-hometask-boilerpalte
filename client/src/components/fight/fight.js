import { controls } from '../../constants/controls';
import { Player } from '../../services/Player';
import { Input } from '../../services/Input';
import { Fight } from '../../services/Fight';

function getIndicatorElementById(id) {
  return document.getElementById(id);
}

export async function fight(firstFighter, secondFighter) {
  const leftHealthIndicator = getIndicatorElementById('left-fighter-indicator');
  const rightHealthIndicator = getIndicatorElementById('right-fighter-indicator');
  
  const fight = new Fight();

  const playerOne = new Player(firstFighter, leftHealthIndicator, fight);
  const playerTwo = new Player(secondFighter, rightHealthIndicator, fight);

  new Input(controls, playerOne, playerTwo, fight);

  while (!fight.isOver) {
    await fightLoop();
  }

  return new Promise((resolve) => {
    resolve(fight);
  });
}

export function getCriticalDamage(attacker) {
  const damage = 2 * attacker.power;
  return damage;
}
export function getDamage(attacker, defender) {  
  const damage = getHitPower(attacker) - getBlockPower(defender);
  
  return damage < 0 ? 0 : damage;
}
export function getHitPower(fighter) {
  const { power } = fighter;
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  return power * criticalHitChance;

}
export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() * (2 - 1) + 1;
  return defense * dodgeChance;
}

function fightLoop(ms = 100) {
  return new Promise(
    resolve => setTimeout(resolve, ms)
  );
}