export class Fight {
    constructor() {
        this.isOver = false;
        this.winner = null;
        this.log = [];
    }
    updateFight(winner) {
        this.isOver = true;
        this.winner = winner;
    }
    addLog(data) {
        this.log = [...this.log, data];
    }
}