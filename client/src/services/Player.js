import { getDamage, getCriticalDamage } from "../components/fight/fight";

export class Player {
    constructor(fighter, healthIndicator, fight) {
        this.fighter = fighter;
        this.healthIndicator = healthIndicator;
        this.fight = fight;
        this.blockOn = false;
        this.health = fighter.health;
        this.criticalAvaible = true;
    }
    hit(opponent) {
        if (this.blockOn) return { damage: 0, health: opponent.health };
        const damage = getDamage(this.fighter, opponent.fighter);

        if (!opponent.blockOn) {
            opponent.health -= damage;
            opponent.setIndicatorWidth(opponent.health);
        }
        if (opponent.health <= 0) {
            this.fight.updateFight(this.fighter);
        }
        return {
            damage,
            health: opponent.health
        }
    }
    criticalHit(opponent) {
        if (this.blockOn || !this.criticalAvaible) return { damage: 0, health: opponent.health };
        const damage = getCriticalDamage(this.fighter);
        opponent.health -= damage;
        opponent.setIndicatorWidth(opponent.health);
        if (opponent.health <= 0) {
            this.fight.updateFight(this.fighter);
        }
        this.criticalAvaible = false;
        setTimeout(() => this.criticalAvaible = true, 10000);
        return {
            damage,
            health: opponent.health
        }
    }
    setIndicatorWidth(health) {
        const healthPercentage = 100 * health / this.fighter.health;
        this.healthIndicator.style.width = `${healthPercentage}%`;
    }
}