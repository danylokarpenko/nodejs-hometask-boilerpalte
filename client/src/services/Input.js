export class Input {
    constructor(controls, playerOne, playerTwo, fight) {
        document.addEventListener('keydown', (event) => {
            const { code } = event;
            const log = {
                fighter1Shot: 0,
                fighter2Shot: 0,
                fighter1Health: 0,
                fighter2Health: 0
            }
            switch (code) {
                case controls.PlayerOneAttack:
                    const { damage: player1Shot, health: player2Health } = playerOne.hit(playerTwo);
                    log.player1Shot = player1Shot;
                    log.player2Health = player2Health;
                    break;
                case controls.PlayerTwoAttack:
                    const { damage: player2Shot, health: player1Health } = playerTwo.hit(playerOne);
                    log.player2Shot = player2Shot;
                    log.player1Health = player1Health;
                    break;
                case controls.PlayerOneBlock:
                    playerOne.blockOn = true;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = true;
                    break;
            }
            fight.addLog(log);
        })
        document.addEventListener('keyup', (event) => {
            const { code } = event;
            switch (code) {
                case controls.PlayerOneBlock:
                    playerOne.blockOn = false;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = false;
                    break;
            }
        })
        this.runOnKeyCombination(() => {
            const log = {
                fighter1Shot: 0,
                fighter2Shot: 0,
                fighter1Health: 0,
                fighter2Health: 0
            }

            const { damage: player1Shot, health: player2Health } = playerOne.criticalHit(playerTwo);
            log.player1Shot = player1Shot;
            log.player2Health = player2Health;

            fight.addLog(log);
        }, controls.PlayerOneCriticalHitCombination);

        this.runOnKeyCombination(() => {
            const log = {
                fighter1Shot: 0,
                fighter2Shot: 0,
                fighter1Health: 0,
                fighter2Health: 0
            }

            const { damage: player2Shot, health: player1Health } = playerTwo.criticalHit(playerOne);
            log.player2Shot = player2Shot;
            log.player1Health = player1Health;

            fight.addLog(log);
        }, controls.PlayerTwoCriticalHitCombination);
    }

    includesCombination(pressedKeys, combination) {
        const isComboPressed = combination.every(key => pressedKeys.has(key));
        return isComboPressed;
    }

    runOnKeyCombination(func, codes) {
        const pressed = new Set();
        document.addEventListener('keydown', function (event) {
            pressed.add(event.code);
            for (let code of codes) {
                if (!pressed.has(code)) {
                    return;
                }
            }
            pressed.clear();
            func();
        });
        document.addEventListener('keyup', function (event) {
            pressed.delete(event.code);
        });
    }
} 