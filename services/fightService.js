const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    create(data) {
        const item = FightRepository.create(data);
        return item;
    }

    findAll() {
        const items = FightRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id, data) {
        const item = this.search({ id });
        if (!item) {
            throw Error('404 User not found');
        }
        const updatedItem = FightRepository.update(id, data);
        if(!updatedItem) {
            return null;
        }
        
        return updatedItem;
    }
    delete(id) {
        const response =  FightRepository.delete(id);
        if(!response) {
            return null;
        }
        return response;
    }
}

module.exports = new FightersService();