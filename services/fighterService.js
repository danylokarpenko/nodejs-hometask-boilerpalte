const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(data) {
        const item = FighterRepository.create(data);
        if (!item) {
            throw Error('Can\'t create fighter');
        }
        return item;
    }

    findAll() {
        const items = FighterRepository.getAll();
        if (!items) {
            throw Error('Can\'t find fighters');
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            throw Error('Fighter not found');
        }
        return item;
    }
    update(id, data) {
        const item = this.search({ id });
        if (!item) {
            throw Error('404 User not found');
        }

        const updatedItem = FighterRepository.update(id, data);
        if (!updatedItem) {
            throw Error('Can\'t update fighter');
        }

        return updatedItem;
    }
    delete(id) {
        const response = FighterRepository.delete(id);
        if (!response) {
            throw Error('Can\'t delete fighter');
        }
        return response;
    }
}

module.exports = new FighterService();